/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.databasepj.service;

import com.pattraporn.databasepj.dao.UserDao;
import com.pattraporn.databasepj.model.User;

/**
 *
 * @author Pattrapon N
 */
public class Userservice {
    public User login(String name , String password){
        UserDao userDao = new UserDao();
        User user = userDao.getbyName(name);
        if(user != null &&user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
